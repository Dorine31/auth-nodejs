const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knex = require('knex')
const cookieParser = require('cookie-parser')
const config = require('./knexfile.js')
const database = knex(config.development)
const app = express(feathers())

//use feathers services
//let userService = service({Model: database, name: 'User'}) 

//Middlewares
const createAccount = require('./middlewares/createAccount')
const confirmRegistration = require('./middlewares/confirmRegistration')
const activateAccount = require('./middlewares/activateAccount')
const login2 = require('./middlewares/login2')
const authorisation = require('./middlewares/authorisation')
const displayUser = require('./middlewares/displayUser')
const displayPicture = require('./middlewares/displayPicture')

//encoded
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.configure(express.rest())

//Before log in
app.post('/create-account', createAccount)

app.get(`/confirm-registration`, confirmRegistration)

app.post('/activate-account', activateAccount)

app.use(cookieParser())

app.post('/login2', login2)

//log in
app.get('/api/users', authorisation)
//app.use(authorisation)999

//is admin
app.use('/api/users', service({
  Model: database,
  name: 'User'
}))

app.use('/api/pictures', service({
  Model: database,
  name: 'Picture'
}))

//static pages
app.use(express.static('./static'))

app.use(express.errorHandler())

//connected on port
const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})