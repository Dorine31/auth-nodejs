const knex = require('knex')
const service = require('feathers-knex')
const bcrypt = require('bcrypt')
let config = require('../knexfile.js')
let database = knex(config.development)
let jwt = require('jsonwebtoken')

module.exports = async (request, response, next) => {
    let email = request.body.email

    //use feathers-knex
    let userService = service({Model: database, name: 'User'})
    let selectUser = await userService.find({ query: {email} })

    let id_user = selectUser[0].id
    let password = selectUser[0].password
    let active = selectUser[0].active

    let pswd = bcrypt.compareSync(request.body.password, password)

    if(pswd == true && active == 1){
      let token = jwt.sign({id:id_user}, 'secret2')
      response.cookie("cookie", token, {maxAge: 10*60*1000})
      response.status(200).json("success")
      next()

    } else {
      response.status(404).json("error")
    }
  }