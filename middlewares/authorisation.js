const knex = require('knex')
const service = require('feathers-knex')
let config = require('../knexfile.js')
let database = knex(config.development)
let jwt = require('jsonwebtoken')
let cookieParser = require('cookie-parser')

module.exports = async (request, response, next) => {
      let cookie = request.cookies.cookie

      let decoded = jwt.verify(cookie, 'secret2')
      let id = decoded["id"]
      
      //use feathers-knex
      let userService = service({Model: database, name: 'User'})
      let selectUser = await userService.find({ query: {id} })
 
      let is_admin = selectUser[0].is_admin
      if(is_admin === 1) {
        next()
      }
      else {
        response.status(403).json("Access denied")
      }
  }