const knex = require('knex')
const service = require('feathers-knex')
let config = require('../knexfile.js')
let database = knex(config.development)
let jwt = require('jsonwebtoken')

module.exports = (request, response, next) => {
        let data = request.body
        let mail = data.email
        let name = data.fullname

        //use feathers-knex
        let userService = service({Model: database, name: 'User'})
        let insertUser = async () => {
            await userService.create({email: mail, fullname: name})
        }
        try{
            insertUser()
            jwt.sign({email:mail}, 'secret', function(err, token){
              console.log(`http://localhost:3000/confirm-registration?email=${mail}&token=${token}`)
              response.redirect('/enregistrement_ok.html')
            })
        } 
        catch {
          response.redirect('/')
        }
        next()
      }