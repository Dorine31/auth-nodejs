const knex = require('knex')
const service = require('feathers-knex')
const bcrypt = require('bcrypt')
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (request, response, next) => {
    let email = request.body.mail
    let saltRounds = 10
    let hash = bcrypt.hashSync(request.body.password, saltRounds)
    try {
      //use feathers-knex
      let userService = service({Model: database, name: 'User'}) //a mettre dans le app.js et à passer en arg. dans les app.use
      let selectUser = await userService.find({ query: {email} })
      let id = selectUser[0].id

      //update data
      await userService.patch(id, {password: hash, active: true})
      response.redirect("/login_ajax.html")
      next()

    } catch (error) {
      response.send(error)
    }
  }